# Makefile for compiling C++ source code.
# Darren Silke (SLKDAR001), 2015

CC=g++ # The compiler name.
CCFLAGS=-std=c++11 # Flags passed to compiler.

# The normal build rules.

samp: Main.o
	$(CC) $(CCFLAGS) Main.o -o samp

Main.o: Main.cpp Audio.h CustomFunctor.h
	$(CC) $(CCFLAGS) Main.cpp -c

# Clean rule.

clean:

	rm -f *.o samp
