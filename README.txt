Author: Darren Silke
Date: 15 May 2015

Name: Assignment 5 - Simple Audio Manipulation Program

Description: See assignment instructions for details.

PLEASE TAKE NOTE:

ASSIGNMENT IS INCOMPLETE. PLEASE LOOK AT CODE FOR FURTHER DETAILS.

Instructions:

1. Open terminal BASH.
2. Type 'make' to compile and link all C++ source files.
3. Refer to code for details.
4. To remove the 'samp' executable and the .o files, type 'make clean'.

List Of Files:

1. README.txt
2. Makefile
3. Audio.h
4. CustomFunctor.h
5. Main.cpp
6. beez18sec_44100_signed_8bit_mono.raw
7. beez18sec_44100_signed_8bit_stereo.raw
8. beez18sec_44100_signed_16bit_mono.raw
9. beez18sec_44100_signed_16bit_stereo.raw 
10. countdown40sec_44100_signed_8bit_mono.raw
11. countdown40sec_44100_signed_8bit_stereo.raw
12. countdown40sec_44100_signed_16bit_mono.raw
13. countdown40sec_44100_signed_16bit_stereo.raw
14. frogs18sec_44100_signed_8bit_mono.raw
15. frogs18sec_44100_signed_8bit_stereo.raw
16. frogs18sec_44100_signed_16bit_mono.raw
17. frogs18sec_44100_signed_16bit_stereo.raw
18. siren40sec_44100_signed_8bit_mono.raw
19. siren40sec_44100_signed_8bit_stereo.raw
20. siren40sec_44100_signed_16bit_mono.raw
21. siren40sec_44100_signed_16bit_stereo.raw

TAKE NOTE OF THE .git FOLDER WHICH CONTAINS ALL INFORMATION RELATING TO THE USE OF GIT FOR A LOCAL REPOSITORY AS REQUIRED BY THIS COURSE.
