/* 
 * File:   CustomFunctor.h
 * Author: Darren Silke
 *
 * Created on 14 May 2015, 3:45 PM
 */

#ifndef CUSTOMFUNCTOR_H
#define	CUSTOMFUNCTOR_H

#include <numeric>

using std::numeric_limits;
using std::pair;

namespace SLKDAR001
{
    /*
     * Mono.
     */
    template <typename T, int N>
    class customFunctor
    {
        public:
            
            const float * ptrToRMSDesired;
            const float * ptrToRMSCurrent;
            
            customFunctor(const float & rmsDesired, const float & rmsCurrent) 
            : ptrToRMSDesired(&rmsDesired), ptrToRMSCurrent(&rmsCurrent)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
            T operator() (const T & inputAmp) const
            {           
                T outputAmp = inputAmp * (*ptrToRMSDesired / *ptrToRMSCurrent);
                int minimumValue = numeric_limits<T>::min();
                int maximumValue = numeric_limits<T>::max();
                                
                if(outputAmp < minimumValue)
                {
                    outputAmp = minimumValue;
                }
                else if(outputAmp > maximumValue)
                {
                    outputAmp = maximumValue;
                }
                
                return outputAmp;
            }
    };  
    
    /*
     * Stereo.
     */
    template <typename T>
    class customFunctor<T, 2>
    {
        public:
                      
            const float * ptrToRMSDesiredLeftChannel;
            const float * ptrToRMSCurrentLeftChannel;
            const float * ptrToRMSDesiredRightChannel;
            const float * ptrToRMSCurrentRightChannel;
            
            customFunctor(const float & rmsDesiredLeftChannel, const float & rmsCurrentLeftChannel, const float & rmsDesiredRightChannel, const float & rmsCurrentRightChannel) 
            : ptrToRMSDesiredLeftChannel(&rmsDesiredLeftChannel), ptrToRMSCurrentLeftChannel(&rmsCurrentLeftChannel), 
              ptrToRMSDesiredRightChannel(&rmsDesiredRightChannel), ptrToRMSCurrentRightChannel(&rmsCurrentRightChannel)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
            pair<T, T> operator() (const pair<T, T> & inputAmp) const
            {           
                pair<T, T> outputAmp;
                
                outputAmp.first = inputAmp.first * (*ptrToRMSDesiredLeftChannel / *ptrToRMSCurrentLeftChannel);
                outputAmp.second = inputAmp.second * (*ptrToRMSDesiredRightChannel / *ptrToRMSCurrentRightChannel);
                
                int minimumValue = numeric_limits<T>::min();
                int maximumValue = numeric_limits<T>::max();
                
                if(outputAmp.first < minimumValue)
                {
                    outputAmp.first = minimumValue;
                }
                else if(outputAmp.first > maximumValue)
                {
                    outputAmp.first = maximumValue;
                }
                
                if(outputAmp.second < minimumValue)
                {
                    outputAmp.second = minimumValue;
                }
                else if(outputAmp.second > maximumValue)
                {
                    outputAmp.second = maximumValue;
                }
                
                return outputAmp;
            } 
    };
}
#endif	/* CUSTOMFUNCTOR_H */
