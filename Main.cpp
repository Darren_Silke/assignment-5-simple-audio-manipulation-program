/* 
 * File:   Main.cpp
 * Author: Darren Silke
 *
 * Created on 11 May 2015, 10:52 AM
 */

#include "Audio.h"

using SLKDAR001::Audio;

/*
 * Main driver function.
 */
int main(int argc, char* argv[]) 
{
    
    Audio<int8_t, 1> beez1(44100, 8, "beez18sec_44100_signed_8bit_mono.raw");
    Audio<int8_t, 2> beez2(44100, 8, "beez18sec_44100_signed_8bit_stereo.raw");
    Audio<int16_t, 1> beez3(44100, 16, "beez18sec_44100_signed_16bit_mono.raw");
    Audio<int16_t, 2> beez4(44100, 16, "beez18sec_44100_signed_16bit_stereo.raw");
    
    Audio<int8_t, 1> countdown1(44100, 8, "countdown40sec_44100_signed_8bit_mono.raw");
    Audio<int8_t, 2> countdown2(44100, 8, "countdown40sec_44100_signed_8bit_stereo.raw");
    Audio<int16_t, 1> countdown3(44100, 16, "countdown40sec_44100_signed_16bit_mono.raw");
    Audio<int16_t, 2> countdown4(44100, 16, "countdown40sec_44100_signed_16bit_stereo.raw");
    
    Audio<int8_t, 1> frogs1(44100, 8, "frogs18sec_44100_signed_8bit_mono.raw");
    Audio<int8_t, 2> frogs2(44100, 8, "frogs18sec_44100_signed_8bit_stereo.raw");
    Audio<int16_t, 1> frogs3(44100, 16, "frogs18sec_44100_signed_16bit_mono.raw");
    Audio<int16_t, 2> frogs4(44100, 16, "frogs18sec_44100_signed_16bit_stereo.raw");
    
    Audio<int8_t, 1> siren1(44100, 8, "siren40sec_44100_signed_8bit_mono.raw");
    Audio<int8_t, 2> siren2(44100, 8, "siren40sec_44100_signed_8bit_stereo.raw");
    Audio<int16_t, 1> siren3(44100, 16, "siren40sec_44100_signed_16bit_mono.raw");
    Audio<int16_t, 2> siren4(44100, 16, "siren40sec_44100_signed_16bit_stereo.raw");
    
    return 0;
}
