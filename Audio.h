/* 
 * File:   Audio.h
 * Author: Darren Silke
 *
 * Created on 11 May 2015, 11:02 AM
 */

#ifndef AUDIO_H
#define	AUDIO_H

#include <vector>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <limits>
#include <algorithm>
#include <numeric>

#include "CustomFunctor.h"

using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::cerr;
using std::move;
using std::pair;
using std::numeric_limits;
using std::reverse;
using std::copy;
using std::back_inserter;
using std::accumulate;
using std::transform;

namespace SLKDAR001
{
    /*
     * Mono.
     */
    template <typename T, int N>
    class Audio
    {
        private:
            
            int sampleRateInHz;
            T  bitCount;   
            int numberOfChannels;
            vector<T> samples;
            
            /*
             * Default constructor to be used internally.
             */
            Audio()
            : sampleRateInHz(0), bitCount(0), numberOfChannels(N), samples(0)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
        public:

            /*
             * 1. Parameterized constructor.
             */
            Audio(int newSampleRateInHz, T newBitCount, const string & inputFileName)
            : sampleRateInHz(newSampleRateInHz), bitCount(newBitCount), numberOfChannels(N), samples(0)
            {
                // Load audio file into samples vector.
                loadAudioFile(inputFileName);
            }

            /*
             * 2. Destructor.
             */
            ~Audio()
            {
                // Leave object in empty state.
                sampleRateInHz = -1;
                bitCount = -1;
                numberOfChannels = -1;
                samples.clear();
            }

            /*
             * 3. Copy constructor.
             */
            Audio(const Audio & rhs)
            : sampleRateInHz(rhs.sampleRateInHz), bitCount(rhs.bitCount), numberOfChannels(rhs.numberOfChannels), samples(rhs.samples)
            {
                /*
                 * Deliberately left empty.
                 */
            }

            /*
             * 4. Move constructor. 
             */
            Audio(Audio && rhs)
            : sampleRateInHz(move(rhs.sampleRateInHz)), bitCount(move(rhs.bitCount)), numberOfChannels(move(rhs.numberOfChannels)), samples(move(rhs.samples))
            {
                // Leave rhs in empty state.
                rhs.sampleRateInHz = -1;
                rhs.bitCount = -1;
                rhs.numberOfChannels = -1;
                rhs.samples.clear();      
            }

            /*
             * 5. Copy assignment operator.
             */
            Audio & operator = (const Audio & rhs)
            {
                if(this != &rhs)
                {
                    // Release current resources.
                    sampleRateInHz = -1;
                    bitCount = -1;
                    numberOfChannels = -1;
                    samples.clear();

                    // Copy data across.
                    sampleRateInHz = rhs.sampleRateInHz;
                    bitCount = rhs.bitCount;
                    numberOfChannels = rhs.numberOfChannels;
                    samples = rhs.samples;
                }
                return *this;
            }

            /*
             * 6. Move assignment operator.
             */
            Audio & operator = (Audio && rhs)
            {
                if(this != &rhs)
                {
                    // Release current resources.
                    sampleRateInHz = -1;
                    bitCount = -1;
                    numberOfChannels = -1;
                    samples.clear();

                    // Steal resources/values from rhs!
                    sampleRateInHz = move(rhs.sampleRateInHz);
                    bitCount = move(rhs.bitCount);
                    numberOfChannels = move(rhs.numberOfChannels);
                    samples = move(rhs.samples);

                    // Leave rhs in empty state.
                    rhs.sampleRateInHz = -1;
                    rhs.bitCount = -1;
                    rhs.numberOfChannels = -1;
                    rhs.samples.clear();
                }
                return *this;
            }

            /*
             * Function to load audio file into memory.
             */
            void loadAudioFile(const string & fileName)
            {
                ifstream inStream(fileName.c_str(), ios::in | ios::binary);
                if(inStream.fail())
                {
                    cerr << "Could not open " << fileName << " for reading.\n";
                    exit(1);
                }

                // Get the file size in bytes.
                inStream.seekg(0, inStream.end);
                int fileSizeInBytes = inStream.tellg();
                inStream.seekg(0, inStream.beg);

                // Allocate memory for the vector of samples.
                int numberOfSamples = fileSizeInBytes / (sizeof(T) * numberOfChannels);
                samples.resize(numberOfSamples);

                // Read data as a block.
                inStream.read((char *)&samples[0], fileSizeInBytes);
                inStream.close();       
            }
            
            /*
             * Function to write audio to file.
             */
            void writeToFile(const string & outputFileName = "Out.raw")
            {                       
                ofstream outStream(outputFileName.c_str(), ios::out | ios::binary);
                if(outStream.fail())
                {
                    cerr << "Could not open " << outputFileName << " for writing.\n";
                    exit(1);
                }
                
                int fileSizeInBytes = samples.size() * (sizeof(T) * numberOfChannels);
                outStream.write((char *)&samples[0], fileSizeInBytes);
                outStream.close();
            }

            /*
             * Overloaded | operator to concatenate two audio files with the 
             * same sampling, sample size and mono/stereo settings.
             */
            Audio operator | (const Audio & rhs)
            {          
                Audio<T, N> result;

                // The concatenated audio file will have the sample sampling, 
                // sample size and mono/stereo settings. The settings can thus 
                // be obtained from either the calling object or the rhs.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                result.samples.reserve(samples.size() + rhs.samples.size());
                // Concatenate samples.
                result.samples.insert(result.samples.end(), samples.begin(), samples.end());
                result.samples.insert(result.samples.end(), rhs.samples.begin(), rhs.samples.end());

                return result;
            }     

            /*
             * Overloaded * operator to volume factor the audio file with 
             * volumeFactor, where volumeFactor is a std::pair<float, float> 
             * with each float value in range [0.0, 1.0].
             */
            Audio operator * (const pair<float, float> volumeFactor)
            {
                Audio<T, N> result;

                // The result audio file will have the same sampling, sample 
                // size and mono/stereo settings. The settings can thus be 
                // obtained from the calling object.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                result.samples.reserve(samples.size());

                for(int i = 0; i < samples.size(); i++)
                {
                    result.samples.push_back(samples[i] * volumeFactor.first);
                }

                return result;      
            }

            /*
             * Overloaded + operator to add audio file amplitudes together (per 
             * sample).
             */
            Audio operator + (const Audio & rhs)
            {
                Audio<T, N> result;

                // The result audio file will have the same sampling, sample 
                // size and mono/stereo settings. The settings can thus be 
                // obtained from either the calling object or the rhs.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                result.samples.reserve(samples.size());
                // Get maximum value of the sample type to be used to clamp each
                // resulting amplitude if greater than this value.
                int maximumValue = numeric_limits<T>::max();

                for(int i = 0; i < samples.size(); i++)
                {
                    if(samples[i] + rhs.samples[i] > maximumValue)
                    {
                        result.samples.push_back(maximumValue);
                    }
                    else
                    {
                        result.samples.push_back(samples[i] + rhs.samples[i]);
                    }
                }              
                return result;        
            }

            /*
             * Overloaded ^ operator to perform a "cut" operation on an audio
             * file. The result is a shorter audio clip (The original audio clip 
             * with a portion removed).
             */
            Audio operator ^ (const pair<int, int> range)
            {
                Audio<T, N> result;

                // The result audio file will have the same sample rate, bit 
                // count and mono/stereo settings. The settings can thus be 
                // obtained from the calling object.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                int newSamplesSize = samples.size() - (range.second - range.first + 1);
                result.samples.reserve(newSamplesSize);

                for(int i = 0; i < samples.size(); i++)
                {
                    if((i < range.first) || (i > range.second))
                    {
                        result.samples.push_back(samples[i]);
                    }
                }         
                return result;     
            }

            /*
             * Function to reverse an audio file.
             */
            void reverseAudioFile()
            {
                reverse(samples.begin(), samples.end());
            }
            
            /*
             * Function to add two (same length) sample ranges from two audio 
             * files. I.E. Add audio file 1 and audio file 2 over sub-ranges 
             * indicated (in seconds). The ranges must be equal in length.
             */
            Audio rangeAdd(const int startRange1, const int endRange1, const int startRange2, const int endRange2, const Audio & rhs)
            {
                if((endRange1 - startRange1 + 1) != (endRange2 - startRange2 + 1))
                {
                    cerr << "Cannot perform operation. Ranges must be the same length.\n";
                    exit(1);
                }
                
                // Preallocate memory.
                int newSamplesSize = (endRange1 - startRange1 + 1) * sampleRateInHz;
                       
                Audio<T, N> temp1;             
                temp1.sampleRateInHz = sampleRateInHz;
                temp1.bitCount = bitCount;
                temp1.numberOfChannels = numberOfChannels;
                temp1.samples.reserve(newSamplesSize);                                     
                copy(samples.begin() + (startRange1 * sampleRateInHz), samples.begin() + ((endRange1 + 1) * sampleRateInHz), back_inserter(temp1.samples));
                
                Audio<T, N> temp2;             
                temp2.sampleRateInHz = rhs.sampleRateInHz;
                temp2.bitCount = rhs.bitCount;
                temp2.numberOfChannels = rhs.numberOfChannels;              
                temp2.samples.reserve(newSamplesSize);
                copy(rhs.samples.begin() + (startRange2 * rhs.sampleRateInHz), rhs.samples.begin() + ((endRange2 + 1) * rhs.sampleRateInHz), back_inserter(temp2.samples));
                        
                Audio<T, N> result = temp1 + temp2;
                
                return result;
            }
            
            /*
             * Function to compute the RMS of an audio file.
             */
            float computeRMS()
            {
                float numberOfSamples = samples.size();          
                auto sum = [numberOfSamples](float sum, T currentValue){return sum + (currentValue * currentValue)/numberOfSamples;};
                
                float rms = accumulate(samples.begin(), samples.end(), 0.0f, sum);
                rms = sqrt(rms);
                
                return rms;
            }
            
            /*
             * Function to perform sound normalization on an audio file. This 
             * function will normalize an audio file to the specified desired 
             * RMS value.
             */
            void normalize(float rmsDesired)
            {         
                float rmsCurrent = computeRMS();
                
                // Preallocate memory.
                vector<T> result;
                result.reserve(samples.size()); 
                
                transform(samples.begin(), samples.end(), back_inserter(result), customFunctor<T, N>(rmsDesired, rmsCurrent)); 
                samples = move(result); 
            }
            
            /*
             * Function to get the sample rate in hertz.
             */
            int getSampleRateInHz()
            {
                return sampleRateInHz;
            }
            
            /*
             * Function to get the bit count.
             */
            T getBitCount()
            {
                return bitCount;
            }
            
            /*
             * Function to get the number of channels.
             */
            int getNumberOfChannels()
            {
                return numberOfChannels;
            }
            
            /*
             * Function to get the samples.
             */
            vector<T> getSamples()
            {
                return samples;
            }
    }; 
        
    /*
     * Stereo.
     */
    template <typename T>
    class Audio<T, 2>
    {
        private:
            
            int sampleRateInHz;
            T  bitCount;   
            int numberOfChannels;
            vector<pair<T, T>> samples;
            
            /*
             * Default constructor to be used internally.
             */
            Audio()
            : sampleRateInHz(0), bitCount(0), numberOfChannels(2), samples(0)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
        public:

            /*
             * 1. Parameterized constructor.
             */
            Audio(int newSampleRateInHz, T newBitCount, const string & inputFileName)
            : sampleRateInHz(newSampleRateInHz), bitCount(newBitCount), numberOfChannels(2), samples(0)
            {
                // Load audio file into samples vector.
                loadAudioFile(inputFileName);
            }
            
            /*
             * 2. Destructor.
             */
            ~Audio()
            {
                // Leave object in empty state.
                sampleRateInHz = -1;
                bitCount = -1;
                numberOfChannels = -1;
                samples.clear();
            }
            
            /*
             * 3. Copy constructor.
             */
            Audio(const Audio & rhs)
            : sampleRateInHz(rhs.sampleRateInHz), bitCount(rhs.bitCount), numberOfChannels(rhs.numberOfChannels), samples(rhs.samples)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
            /*
             * 4. Move constructor.
             */
            Audio(Audio && rhs)
            : sampleRateInHz(move(rhs.sampleRateInHz)), bitCount(move(rhs.bitCount)), numberOfChannels(move(rhs.numberOfChannels)), samples(move(rhs.samples))
            {
                // Leave rhs in empty state.
                rhs.sampleRateInHz = -1;
                rhs.bitCount = -1;
                rhs.numberOfChannels = -1;
                rhs.samples.clear();      
            }
            
            /*
             * 5. Copy assignment operator.
             */
            Audio & operator = (const Audio & rhs)
            {
                if(this != &rhs)
                {
                    // Release current resources.
                    sampleRateInHz = -1;
                    bitCount = -1;
                    numberOfChannels = -1;
                    samples.clear();

                    // Copy data across.
                    sampleRateInHz = rhs.sampleRateInHz;
                    bitCount = rhs.bitCount;
                    numberOfChannels = rhs.numberOfChannels;
                    samples = rhs.samples;
                }
                return *this;
            }
            
            /*
             * 6. Move assignment operator.
             */
            Audio & operator = (Audio && rhs)
            {
                if(this != &rhs)
                {
                    // Release current resources.
                    sampleRateInHz = -1;
                    bitCount = -1;
                    numberOfChannels = -1;
                    samples.clear();

                    // Steal resources/values from rhs!
                    sampleRateInHz = move(rhs.sampleRateInHz);
                    bitCount = move(rhs.bitCount);
                    numberOfChannels = move(rhs.numberOfChannels);
                    samples = move(rhs.samples);

                    // Leave rhs in empty state.
                    rhs.sampleRateInHz = -1;
                    rhs.bitCount = -1;
                    rhs.numberOfChannels = -1;
                    rhs.samples.clear();
                }
                return *this;
            }
                   
            /*
             * Function to load audio file into memory.
             */
            void loadAudioFile(const string & fileName)
            {
                ifstream inStream(fileName.c_str(), ios::in | ios::binary);
                if(inStream.fail())
                {
                    cerr << "Could not open " << fileName << " for reading.\n";
                    exit(1);
                }

                // Get the file size in bytes.
                inStream.seekg(0, inStream.end);
                int fileSizeInBytes = inStream.tellg();
                inStream.seekg(0, inStream.beg);

                // Allocate memory for the vector of samples.
                int numberOfSamples = fileSizeInBytes / (sizeof(T) * numberOfChannels);
                samples.resize(numberOfSamples);

                // Read data as a block.
                inStream.read((char *)&samples[0], fileSizeInBytes);
                inStream.close();       
            }
            
            /*
             * Function to write audio to file.
             */
            void writeToFile(const string & outputFileName = "Out.raw")
            {                       
                ofstream outStream(outputFileName.c_str(), ios::out | ios::binary);
                if(outStream.fail())
                {
                    cerr << "Could not open " << outputFileName << " for writing.\n";
                    exit(1);
                }
                
                int fileSizeInBytes = samples.size() * (sizeof(T) * numberOfChannels);
                outStream.write((char *)&samples[0], fileSizeInBytes);
                outStream.close();
            }
            
            /*
             * Overloaded | operator to concatenate two audio files with the 
             * same sampling, sample size and mono/stereo settings.
             */
            Audio operator | (const Audio & rhs)
            {          
                Audio<T, 2> result;

                // The concatenated audio file will have the sample sampling, 
                // sample size and mono/stereo settings. The settings can thus 
                // be obtained from either the calling object or the rhs.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                result.samples.reserve(samples.size() + rhs.samples.size());
                // Concatenate samples.
                result.samples.insert(result.samples.end(), samples.begin(), samples.end());
                result.samples.insert(result.samples.end(), rhs.samples.begin(), rhs.samples.end());

                return result;
            } 
            
            /*
             * Overloaded * operator to volume factor the audio file with 
             * volumeFactor, where volumeFactor is a std::pair<float, float> 
             * with each float value in range [0.0, 1.0].
             */
            Audio operator * (const pair<float, float> volumeFactor)
            {
                Audio<T, 2> result;

                // The result audio file will have the same sampling, sample 
                // size and mono/stereo settings. The settings can thus be 
                // obtained from the calling object.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                result.samples.reserve(samples.size());

                for(int i = 0; i < samples.size(); i++)
                {
                    pair<T, T> temp;
                    temp.first = samples[i].first * volumeFactor.first;
                    temp.second = samples[i].second * volumeFactor.second;
                    result.samples.push_back(temp);
                }

                return result;      
            }
                   
            /*
             * Overloaded + operator to add audio file amplitudes together (per 
             * sample).
             */
            Audio operator + (const Audio & rhs)
            {
                Audio<T, 2> result;

                // The result audio file will have the same sampling, sample 
                // size and mono/stereo settings. The settings can thus be 
                // obtained from either the calling object or the rhs.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                result.samples.reserve(samples.size());
                // Get maximum value of the sample type to be used to clamp each
                // resulting amplitude if greater than this value.
                int maximumValue = numeric_limits<T>::max();

                for(int i = 0; i < samples.size(); i++)
                {
                    pair<T, T> temp;
                    
                    if(samples[i].first + rhs.samples[i].first > maximumValue)
                    {
                        temp.first = maximumValue;
                    }
                    else
                    {
                        temp.first = samples[i].first + rhs.samples[i].first;
                    }
                    
                    if(samples[i].second + rhs.samples[i].second > maximumValue)
                    {
                        temp.second = maximumValue;
                    }
                    else
                    {
                        temp.second = samples[i].second + rhs.samples[i].second;
                    }
                    
                    result.samples.push_back(temp);
                }              
                return result;        
            }
            
            /*
             * Overloaded ^ operator to perform a "cut" operation on an audio
             * file. The result is a shorter audio clip (The original audio clip 
             * with a portion removed).
             */
            Audio operator ^ (const pair<int, int> range)
            {
                Audio<T, 2> result;

                // The result audio file will have the same sample rate, bit 
                // count and mono/stereo settings. The settings can thus be 
                // obtained from the calling object.
                result.sampleRateInHz = sampleRateInHz;
                result.bitCount = bitCount;
                result.numberOfChannels = numberOfChannels;

                // Preallocate memory.
                int newSamplesSize = samples.size() - (range.second - range.first + 1);
                result.samples.reserve(newSamplesSize);

                for(int i = 0; i < samples.size(); i++)
                {
                    if((i < range.first) || (i > range.second))
                    {
                        result.samples.push_back(samples[i]);
                    }
                }         
                return result;     
            }
                        
            /*
             * Function to reverse an audio file.
             */
            void reverseAudioFile()
            {
                reverse(samples.begin(), samples.end());
            }
            
            /*
             * Function to add two (same length) sample ranges from two audio 
             * files. I.E. Add audio file 1 and audio file 2 over sub-ranges 
             * indicated (in seconds). The ranges must be equal in length.
             */
            Audio rangeAdd(const int startRange1, const int endRange1, const int startRange2, const int endRange2, const Audio & rhs)
            {
                if((endRange1 - startRange1 + 1) != (endRange2 - startRange2 + 1))
                {
                    cerr << "Cannot perform operation. Ranges must be the same length.\n";
                    exit(1);
                }
                
                // Preallocate memory.
                int newSamplesSize = (endRange1 - startRange1 + 1) * sampleRateInHz;
                       
                Audio<T, 2> temp1;             
                temp1.sampleRateInHz = sampleRateInHz;
                temp1.bitCount = bitCount;
                temp1.numberOfChannels = numberOfChannels;
                temp1.samples.reserve(newSamplesSize);                                     
                copy(samples.begin() + (startRange1 * sampleRateInHz), samples.begin() + ((endRange1 + 1) * sampleRateInHz), back_inserter(temp1.samples));
                
                Audio<T, 2> temp2;             
                temp2.sampleRateInHz = rhs.sampleRateInHz;
                temp2.bitCount = rhs.bitCount;
                temp2.numberOfChannels = rhs.numberOfChannels;              
                temp2.samples.reserve(newSamplesSize);
                copy(rhs.samples.begin() + (startRange2 * rhs.sampleRateInHz), rhs.samples.begin() + ((endRange2 + 1) * rhs.sampleRateInHz), back_inserter(temp2.samples));
                        
                Audio<T, 2> result = temp1 + temp2;
                
                return result;
            }
            
            /*
             * Function to compute the RMS of the left channel of an audio file.
             */
            float computeRMSLeftChannel()
            {
                float numberOfSamples = samples.size();          
                auto sum = [numberOfSamples](float sum, pair<T, T> currentValue){return sum + (currentValue.first * currentValue.first)/numberOfSamples;};
                
                float rms = accumulate(samples.begin(), samples.end(), 0.0f, sum);
                rms = sqrt(rms);
                
                return rms;
            }
            
            /*
             * Function to compute the RMS of the right channel of an audio 
             * file.
             */
            float computeRMSRightChannel()
            {
                float numberOfSamples = samples.size();
                auto sum = [numberOfSamples](float sum, pair<T, T> currentValue){return sum + (currentValue.second * currentValue.second)/numberOfSamples;};
                
                float rms = accumulate(samples.begin(), samples.end(), 0.0f, sum);
                rms = sqrt(rms);
                
                return rms;
            }
            
            /*
             * Function to perform sound normalization on an audio file. This 
             * function will normalize an audio file to the specified desired 
             * RMS values for the left and right channels.
             */
            void normalize(float rmsDesiredLeftChannel, float rmsDesiredRightChannel)
            {         
                float rmsCurrentLeftChannel = computeRMSLeftChannel();
                float rmsCurrentRightChannel = computeRMSRightChannel();
                
                // Preallocate memory.
                vector<pair<T, T>> result;
                result.reserve(samples.size()); 
                
                transform(samples.begin(), samples.end(), back_inserter(result), customFunctor<T, 2>(rmsDesiredLeftChannel, rmsCurrentLeftChannel, rmsDesiredRightChannel, rmsCurrentRightChannel)); 
                samples = move(result); 
            }
            
            /*
             * Function to get the sample rate in hertz.
             */
            int getSampleRateInHz()
            {
                return sampleRateInHz;
            }
            
            /*
             * Function to get the bit count.
             */
            T getBitCount()
            {
                return bitCount;
            }
            
            /*
             * Function to get the number of channels.
             */
            int getNumberOfChannels()
            {
                return numberOfChannels;
            }
            
            /*
             * Function to get the samples.
             */
            vector<pair<T, T>> getSamples()
            {
                return samples;
            }
    };
}
#endif	/* AUDIO_H */
